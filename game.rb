class Game
  def initialize
    @board = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
    @com = "X" # the computer's marker
    @hum = "O" # the user's marker
    @game_type = nil
    @hard_game = true
  end

  def start_game
    choose_game_type

    if @game_type == 3
      choose_game_difficulty
    end
    
    # start by printing the board
    print_board
    print_available_spots(@board)

    # loop through until the game was won or tied
    until game_is_over(@board) || tie(@board)

      get_human_spot(@game_type) if @game_type == 1 || @game_type == 3
      eval_board(@hum, @com) if @game_type == 2
      if !game_is_over(@board) && !tie(@board)
        if @game_type == 1
          print_board
          print_available_spots(@board)
          get_human_spot(@game_type, 2)
        else
          eval_board(@com, @hum)
        end
      end
      print_board
      print_available_spots(@board)
    end

    puts "Game over"
  end

  private
  
  def print_board
    puts " #{@board[0]} | #{@board[1]} | #{@board[2]} \n===+===+===\n #{@board[3]} | #{@board[4]} | #{@board[5]} \n===+===+===\n #{@board[6]} | #{@board[7]} | #{@board[8]} \n"
  end

  def game_type_options
    puts "Please, select the type of game option:"
    puts "1 - human vs. human"
    puts "2 - computer vs. computer"
    puts "3 - human vs. computer\n\n"
  end

  def game_difficulty_options
    puts "Please, select the game difficulty:"
    puts "1 - easy"
    puts "2 - hard\n\n"
  end

  def choose_game_type
    good_choose = nil
    game_type_options

    until good_choose
      @game_type = gets.chomp.to_i

      if (1..3).include?(@game_type)
        good_choose = true
      else
        puts "Invalid option"
        puts "===+===+===\n\n"
        game_type_options
      end
    end
  end

  def choose_game_difficulty
    good_choose = nil
    game_difficulty_options

    until good_choose
      game_difficulty = gets.chomp.to_i

      if (1..2).include?(game_difficulty)
        good_choose = true
        game_difficulty == 1 ? @hard_game = false : @hard_game = true
      else
        puts "Invalid option"
        puts "===+===+===\n\n"
        game_difficulty_options
      end
    end
  end

  def print_available_spots(board)
    available = board.select { |spot| spot != "X" && spot != "O" }

    if available.size == 9
      puts "Enter [0-8]:"
    elsif available.size > 0
      puts "Enter [#{available.join(';')}]"      
    end
  end

  def get_human_spot(game_type, player = 1)
    spot = nil
    until spot
      spot = gets.chomp.to_i
      if available_spot?(spot)
        if player == 1
          @board[spot] = @hum
        else
          @board[spot] = @com
        end
      else
        spot_message(spot)
        spot = nil
      end
    end
  end

  def available_spot?(spot)
    if spot < 0 || spot > 8
      return false
    else
      if @board[spot] != "X" && @board[spot] != "O"
        return true
      else
        return false
      end
    end
  end

  def spot_message(spot)
    if spot < 0 || spot > 8
      puts "Invalid spot! Please, Enter [0-8]:\n"
    else
      if @board[spot] != "X" && @board[spot] != "O"
        return true
      elsif @board[spot] == "O"
        puts "Invalid spot! You have already chosen this spot.\n"
      elsif @board[spot] == "X"
        puts "Invalid spot! Other player have already chosen this spot.\n"
      end
    end
  end

  def eval_board(player, opponent)
    spot = nil
    until spot
      if @hard_game
        if @board[4] == "4"
          spot = 4
          @board[spot] = player
        else
          spot = get_best_move(@board, player, opponent)
          if @board[spot] != "X" && @board[spot] != "O"
            @board[spot] = player
          else
            spot = nil
          end
        end
      else
        spot = get_best_move(@board, player, opponent)
        if @board[spot] != "X" && @board[spot] != "O"
          @board[spot] = player
        else
          spot = nil
        end
      end
    end
  end
  
  def get_best_move(board, next_player, opponent, depth = 0, best_score = {})
  available_spaces = []
    best_move = nil
    board.each do |spot|
      if spot != "X" && spot != "O"
        available_spaces << spot
      end
    end

    available_spaces.each do |available_space|
      board[available_space.to_i] = next_player
      if game_is_over(board)
        best_move = available_space.to_i
        board[available_space.to_i] = available_space
        return best_move
      else
        board[available_space.to_i] = opponent
        if game_is_over(board)
          best_move = available_space.to_i
          board[available_space.to_i] = available_space
          return best_move
        else
          board[available_space.to_i] = available_space
        end
      end
    end
    if best_move
      return best_move
    else
      n = rand(0..available_spaces.count)
      return available_spaces[n].to_i
    end
  end

  def game_is_over(b)

    [b[0], b[1], b[2]].uniq.length == 1 ||
    [b[3], b[4], b[5]].uniq.length == 1 ||
    [b[6], b[7], b[8]].uniq.length == 1 ||
    [b[0], b[3], b[6]].uniq.length == 1 ||
    [b[1], b[4], b[7]].uniq.length == 1 ||
    [b[2], b[5], b[8]].uniq.length == 1 ||
    [b[0], b[4], b[8]].uniq.length == 1 ||
    [b[2], b[4], b[6]].uniq.length == 1
  end

  def tie(b)
    b.all? { |s| s == "X" || s == "O" }
  end
end

game = Game.new
game.start_game
